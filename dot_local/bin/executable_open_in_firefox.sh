#!/bin/sh

if [[ $# -ne 1 ]]; then
	echo "usage: $0 <url>"
fi
fixed_url="$(echo "$1" | sed -e 's/^ftls:/https:/' -e 's/^ftl:/http:/')"
firefox "$fixed_url"
