#!/bin/bash

w=$(find $HOME/.wallpapers/ -name *.jpg | sort -R | head -1)
[ -f $w ] || exit 99
feh --bg-fill $w
