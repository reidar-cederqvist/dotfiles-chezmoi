#!/usr/bin/bash
#

killall -9 polybar
sleep 0.5

OIFS=$IFS
IFS='
'
for m in $(polybar -M); do
    if [[ "$m" == *"primary"* ]]; then
        MONITOR=${m%%:*} polybar --reload main &
    else
        MONITOR=${m%%:*} polybar --reload secondary &
    fi
done

IFS=$OIFS
