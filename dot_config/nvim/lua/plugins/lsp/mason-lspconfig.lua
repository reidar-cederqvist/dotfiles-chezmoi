return {
	"williamboman/mason-lspconfig.nvim",
	opts = {
		-- list of servers for mason to install
		ensure_installed = {
			"ansiblels",
			"bashls",
			"clangd",
			"cssls",
			"dockerls",
			"html",
			"eslint",
			"ts_ls",
			"jsonls",
			"ast_grep",
			"lua_ls",
			"grammarly",
			"remark_ls",
			"pyright",
			"gitlab_ci_ls",
			"yamlls",
		},
	},
}
