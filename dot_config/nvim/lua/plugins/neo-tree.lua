return {
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"nvim-tree/nvim-web-devicons",
		"MunifTanjim/nui.nvim",
		"3rd/image.nvim",
	},
	opts = {
		filesystem = {
			filtered_items = {
				visible = true,
				hide_dotfiles = true,
				hide_gitignored = true,
			},
		},
	},
	init = function()
		vim.keymap.set("n", "<leader>ee", ":Neotree toggle left<CR>")
		vim.keymap.set("n", "<leader>ef", ":Neotree filesystem reveal left<CR>")
	end,
}
