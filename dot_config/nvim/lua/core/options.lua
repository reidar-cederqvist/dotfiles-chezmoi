vim.g.mapleader = " "
vim.g.maplocalleader = " "

local opt = vim.opt

opt.title = true
opt.showmode = false -- Dont show mode since we have a statusline
opt.mouse = "a" -- Enable mouse mode
opt.hlsearch = true --highligt search
opt.relativenumber = true -- Relative line numbers
opt.number = true -- Print line number
opt.autoindent = true -- keep indentation from previous line (default true)
opt.ww = "l,h,b,s,<,>,[<]"
opt.breakindent = true --indent wraped lines
opt.undofile = true -- Save undo history
opt.ignorecase = true -- Ignore case

opt.smartcase = true -- Don't ignore case with capitals
opt.showmatch = true --show in split all matches when doing substitutions
opt.incsearch = true --show current search pattern highligted

opt.autowrite = true -- Enable auto write
opt.timeoutlen = 300 -- Lower than default (1000) to quickly trigger which-key

opt.splitright = true -- Put new windows right of current
opt.splitbelow = true -- Put new windows below current

opt.list = true -- Show some invisible characters (tabs...
opt.listchars = { tab = "» ", trail = "·", nbsp = "␣" }

opt.inccommand = "split" -- preview incremental substitute

opt.cursorline = true -- Enable highlighting of the current line
opt.scrolloff = 10 -- Lines of context
opt.sidescrolloff = 15 -- Columns of context

opt.tabstop = 2 -- Number of spaces tabs count for
opt.formatoptions = "jcrqlnt" -- tcqj
opt.laststatus = 3 -- global statusline

opt.linebreak = true -- Wrap lines at convenient points
opt.shiftround = true -- Round indent to closest tabstop
opt.signcolumn = "yes" -- always show the sign column so it doesn't shift around
opt.smartindent = true -- Insert indents automatically
opt.spelllang = { "en", "sv" }
opt.splitkeep = "screen"
opt.termguicolors = true -- True color support
opt.background = "dark" -- colorschemes that can be light or dark will be dark
opt.undolevels = 10000
opt.swapfile = true -- do not use swap file
opt.directory = "/home/reidar/.cache/nvim/swap"
opt.virtualedit = "block" -- Allow cursor to move where there is no text in visual block mode
opt.winminwidth = 5 -- Minimum window width
opt.wrap = true -- Enable line wrap

-- Fix markdown indentation settings
vim.g.markdown_recommended_style = 0
-- vim: ts=2 sts=2 sw=2 et

vim.api.nvim_create_user_command("FormatDisable", function(args)
	if args.bang then
		-- FormatDisable! will disable formatting just for this buffer
		vim.b.disable_autoformat = true
	else
		vim.g.disable_autoformat = true
	end
end, {
	desc = "Disable autoformat-on-save",
	bang = true,
})
vim.api.nvim_create_user_command("FormatEnable", function()
	vim.b.disable_autoformat = false
	vim.g.disable_autoformat = false
end, {
	desc = "Re-enable autoformat-on-save",
})
