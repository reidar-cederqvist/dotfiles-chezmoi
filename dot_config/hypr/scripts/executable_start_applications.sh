#!/bin/bash
#

hyprpaper &
waybar &
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
count=10
while ! "$SCRIPT_DIR"/test.py && [[ $count -gt 0 ]]; do
	sleep 1
	count=$((count-1))
done
dunst &
wl-paste --watch cliphist store &
pasystray &
udiskie -t &
slack -u &
ckb-next -b &
nm-applet &
nextcloud &
sleep 1
if ! pidof waybar 2>&1 >/dev/null; then
	waybar &
fi
