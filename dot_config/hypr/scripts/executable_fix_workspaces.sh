#!/bin/bash
#

CONF="$HOME/.config/hypr"
MON_C="$CONF/conf/monitors.conf"

find_monitor() {
  monitor=$(hyprctl -j monitors | jq ".[] | select(.serial == \"$1\").name" | xargs)
  if [ -z "$monitor" ]; then
    echo "$1"
  else
    echo "$monitor"
  fi
}

if [ $# -ne 2 ]; then
  echo -e "Usage:
\t$0 <Main monitor> <Second monitor>"
  exit 99
fi

main_monitor=$(find_monitor "$1")
sec_monitor=$(find_monitor "$2")

if grep -q "mainMonitor = $main_monitor" "$MON_C" && grep -q "secMonitor = $sec_monitor" "$MON_C"; then
  exit 0
fi

active_window=$(hyprctl -j activewindow | jq .address | xargs)

sed -i "s/\$mainMonitor = .*$/\$mainMonitor = $main_monitor/" "$MON_C"
sed -i "s/\$secMonitor = .*$/\$secMonitor = $sec_monitor/" "$MON_C"

cmd="${cmd} dispatch focuswindow address:$active_window"

hyprctl --batch "$cmd"

bash "$CONF/scripts/set_wallpaper.sh"
