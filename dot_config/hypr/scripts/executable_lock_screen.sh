#!/bin/bash
#
type playerctl >/dev/null 2>&1
has_playerctl=$?
playing=false

if [[ "$has_playerctl" = 0 ]]; then
	 if [[ "$(playerctl status)" = "Playing" ]]; then
		 playerctl pause
		 playing=true
	 fi
fi

hyprlock

if [[ "$has_playerctl" = 0 ]] && $playing; then
	playerctl play
fi
