#!/bin/bash

PROG="/usr/bin/hyprpaper"
WAL_DIR="$HOME/.wallpapers"
BLURED="$HOME/.lock-screen-image.png"

[ -f "$PROG" ] || exit 99
pidof "$PROG" || {
  $PROG &
  sleep 0.1
}

# preload a random image
img=""

if command -v fd &>/dev/null; then
  img=$(fd -a --base-directory "$WAL_DIR" -e jpg -e jpeg -e png | sort -R | tail -1)
else
  img=$(find "$WAL_DIR" -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" \) | sort -R | tail -1)
fi

echo "$img"

[ -f "$img" ] || exit 99

i=0
while ! hyprctl hyprpaper preload "$img"; do
  sleep 0.1
  i=$((i + 1))
  if [[ $i -gt 20 ]]; then
    echo "could not preload $img"
    exit 99
  fi
done

monitors=$(hyprctl -j monitors | jq ".[].name" | xargs)
for monitor in $monitors; do
  hyprctl hyprpaper wallpaper "$monitor,$img"
done

sleep 0.1
i=0
while ! hyprctl hyprpaper unload "$img"; do
  sleep 0.1
  i=$((i + 1))
  if [[ $i -gt 20 ]]; then
    echo "could not properly unload $img"
    break
  fi
done

if command -v magick &>/dev/null; then
  magick "$img" -blur 0x2 -alpha off "$BLURED"
fi
